VERSION = '0.1.0'

local config = import('micro/config')

config.AddRuntimeFile('xonsh', config.RTSyntax, 'syntax/xonsh.yaml')
