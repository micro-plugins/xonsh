# Xonsh - Plugin for [micro editor](https://micro-editor.github.io)

Syntax highlighting for **[xonsh](https://xon.sh/)** files

![Syntax Highlight](./assets/xonsh.png)

## Installation

### Settings

Add this repo as a **pluginrepos** option in the **~/.config/micro/settings.json**
file (it is necessary to restart the micro after this change):

```json
{
  "pluginrepos": [
      "https://codeberg.org/micro-plugins/xonsh/raw/branch/main/repo.json"
  ]
}
```

### Install

In your micro editor press **Ctrl-e** and run command:

```
> plugin install xonsh
```

or run in your shell

```sh
micro -plugin install xonsh
```
